package module2;

public class ClassA {

	ClassA() {
		System.out.println("constructorA");
	}

	public void methodA1(String A,String A1) {
		System.out.println("methodA1");
	}

	public void methodA2(String A,String A1,String A2) {
		System.out.println("methodA2");
	}

	public void methodA3(String A,String A1,String A2,String A3) {
		System.out.println("methodA3");
	}

	public void methodA4(String A,String A1,String A2,String A3,String A4) {
		System.out.println("methodA4");
	}

	private void methodA5() {
		System.out.println("methodA5");
	}

	protected void methodA6() {
		System.out.println("methodA6");
	}

}
