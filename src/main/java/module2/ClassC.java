package module2;

public class ClassC {
	ClassC() {
		System.out.println("constructorC");
	}

	public void methodC1() {
		System.out.println("methodC1");
	}

	public void methodC2() {
		System.out.println("methodC2");
	}

	public void methodC3() {
		System.out.println("methodC3");
	}

	public void methodC4() {
		System.out.println("methodC4");
	}

	private void methodC5() {
		System.out.println("methodC5");
	}

	protected void methodC6() {
		System.out.println("methodC6");
	}
}
