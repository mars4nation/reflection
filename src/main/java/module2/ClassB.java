package module2;

public class ClassB {

	ClassB() {
		System.out.println("constructorB");
	}

	public void methodB1() {
		System.out.println("methodB1");
	}

	public void methodB2() {
		System.out.println("methodB2");
	}

	public void methodB3() {
		System.out.println("methodB3");
	}

	public void methodB4() {
		System.out.println("methodB4");
	}

	private void methodB5() {
		System.out.println("methodB5");
	}

	protected void methodB6() {
		System.out.println("methodB6");
	}
	
}
