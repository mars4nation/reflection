package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadPropertyFile {

	private static final String ENV_PROPERTIES = "./src/main/resources/config/env.properties";
	private Properties properties = new Properties();

	private Properties loadPropertiesFile() {
		Properties result = new Properties();
		try {
			String filename = getPropertyOrNull(ENV_PROPERTIES);
			if (filename == null) {
				filename = ENV_PROPERTIES;
			}
			InputStream stream = getClass().getClassLoader().getResourceAsStream(filename);
			if (stream == null) {
				stream = new FileInputStream(new File(filename));
			}
			result.load(stream);
		} catch (IOException e) {
			// throw new UnknownPropertyException("Property file is not found");
		}
		return result;
	}

	public String getPropertyOrNull(String name) {
		return getProperty(name, false);
	}

	private String getProperty(String name, boolean forceExceptionIfNotDefined) {
		String result;
		if ((result = System.getProperty(name, null)) != null && result.length() > 0) {
			return result;
		} else if ((result = getPropertyFromPropertiesFile(name)) != null && result.length() > 0) {
			return result;
		} else if (forceExceptionIfNotDefined) {
			// throw new UnknownPropertyException("Unknown property: [" + name +
			// "]");
		}
		return result;
	}

	private String getPropertyFromPropertiesFile(String name) {
		Object result = properties.get(name);
		if (result == null) {
			return null;
		} else {
			return result.toString();
		}
	}

}
