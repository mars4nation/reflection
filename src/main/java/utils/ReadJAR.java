package utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ReadJAR {
	public static ArrayList<String> readJAR(String jarPath) throws SecurityException, ClassNotFoundException {
		ArrayList<String> classList = new ArrayList<String>();

		String actualName = null;
		try {
			JarFile jarfile = new JarFile(jarPath);
			Enumeration<JarEntry> entries = jarfile.entries();

			while (entries.hasMoreElements()) {

				JarEntry entry = (JarEntry) entries.nextElement();
				String entryName = entry.getName();

				if (entryName.endsWith(".class")) {
					System.out.println(entryName);
					actualName = entryName.replaceAll("/", ".").substring(0, entryName.lastIndexOf("."));
					classList.add(actualName);
					System.out.println(actualName);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return classList;
	}
}
