package module3;

public class ClassE {

	ClassE() {
		System.out.println("constructorE");
	}

	public void methodE1() {
		System.out.println("methodE1");
	}

	public void methodE2() {
		System.out.println("methodE2");
	}

	public void methodE3() {
		System.out.println("methodE3");
	}

	public void methodE4() {
		System.out.println("methodE4");
	}

	private void methodE5() {
		System.out.println("methodE5");
	}

	protected void methodE6() {
		System.out.println("methodE6");
	}

}
