package module3;

public class ClassF {

	ClassF() {
		System.out.println("constructorC");
	}

	public void methodF1() {
		System.out.println("methodF1");
	}

	public void methodF2() {
		System.out.println("methodF2");
	}

	public void methodF3() {
		System.out.println("methodF3");
	}

	public void methodF4() {
		System.out.println("methodF4");
	}

	private void methodF5() {
		System.out.println("methodF5");
	}

	protected void methodF6() {
		System.out.println("methodF6");
	}

}
