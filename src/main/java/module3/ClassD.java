package module3;

public class ClassD {
	ClassD() {
		System.out.println("constructorD");
	}

	public void methodD1() {
		System.out.println("methodD1");
	}

	public void methodD2() {
		System.out.println("methodD2");
	}

	public void methodD3() {
		System.out.println("methodD3");
	}

	public void methodD4() {
		System.out.println("methodD4");
	}

	private void methodD5() {
		System.out.println("methodD5");
	}

	protected void methodD6() {
		System.out.println("methodD6");
	}
}
