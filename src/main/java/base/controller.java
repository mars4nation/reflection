package base;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import utils.ReadJAR;

import java.util.Set;

public class controller {

	private static HashMap<Method, Class<?>[]> mapping = new HashMap<Method, Class<?>[]>();
	private static ArrayList<String> classList = new ArrayList<String>();
	private static String className = null;
	private static ArrayList<Method> methodList = new ArrayList<Method>();

	public static void getMethods(String className) {
		try {
			Method[] methods = Class.forName(className).getDeclaredMethods();
			for (Method method : methods) {
				int modifiers = method.getModifiers();
				Class<?>[] parameters = method.getParameterTypes();
				if (Modifier.isPublic(modifiers) || Modifier.isProtected(modifiers)) {
					mapping.put(method, parameters);
					methodList.add(method);
					System.out.println(method);
					// System.out.println(parameters);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showMapping() {
		Set<Map.Entry<Method, Class<?>[]>> entrySet = mapping.entrySet();
		for (Entry entry : entrySet) {
			System.out.println("Method: " + entry.getKey() + "  Parameters: " + entry.getValue().toString());
		}
	}

	public static void main(String[] args) throws SecurityException, ClassNotFoundException {
		String jarPath = "/home/riteshvashisth/myspace/src/main/resources/Reflection-0.0.1-SNAPSHOT.jar";
		// showMapping();
		classList = ReadJAR.readJAR(jarPath);
		className = classList.get(2);
		getMethods(className);
	}

}